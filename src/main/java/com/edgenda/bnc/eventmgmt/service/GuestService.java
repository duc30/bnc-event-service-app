package com.edgenda.bnc.eventmgmt.service;

import com.edgenda.bnc.eventmgmt.model.Event;
import com.edgenda.bnc.eventmgmt.model.Guest;
import com.edgenda.bnc.eventmgmt.repository.EventRepository;
import com.edgenda.bnc.eventmgmt.repository.GuestRepository;
import com.edgenda.bnc.eventmgmt.service.exception.UnknownGuestException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class GuestService {

    private final GuestRepository skillRepository;

    private final EventRepository employeeRepository;

    @Autowired
    public GuestService(GuestRepository skillRepository, EventRepository employeeRepository) {
        this.skillRepository = skillRepository;
        this.employeeRepository = employeeRepository;
    }

    public Guest getSkill(Long id) {
        Assert.notNull(id, "Guest ID cannot be null");
        return skillRepository.findById(id)
                .orElseThrow(() -> new UnknownGuestException(id));
    }

    public List<Guest> getSkills() {
        return skillRepository.findAll();
    }

    public List<Event> getEmployeesWithSkill(Long skillId) {
        Assert.notNull(skillId, "Guest ID cannot be null");
        return employeeRepository.findByGuestId(skillId);
    }
}

