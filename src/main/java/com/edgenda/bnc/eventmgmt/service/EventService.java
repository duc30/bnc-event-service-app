package com.edgenda.bnc.eventmgmt.service;

import com.edgenda.bnc.eventmgmt.model.Event;
import com.edgenda.bnc.eventmgmt.model.Guest;
import com.edgenda.bnc.eventmgmt.repository.EventRepository;
import com.edgenda.bnc.eventmgmt.repository.GuestRepository;
import com.edgenda.bnc.eventmgmt.service.exception.UnknownEventException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import javax.transaction.Transactional;
import java.util.Collections;
import java.util.List;

@Service
@Transactional
public class EventService {

    private final EventRepository eventRepository;

    private final GuestRepository guestRepository;

    @Autowired
    public EventService(EventRepository employeeRepository, GuestRepository skillRepository) {
        this.eventRepository = employeeRepository;
        this.guestRepository = skillRepository;
    }

    public Event getEmployee(Long id) {
        Assert.notNull(id, "Employee ID cannot be null");
        return eventRepository.findById(id)
                .orElseThrow(() -> new UnknownEventException(id));
    }

    public List<Event> getEvents() {
        return eventRepository.findAll();
    }

    public Event createEmployee(Event employee) {
        Assert.notNull(employee, "Employee cannot be null");
        final Event newEmployee = new Event(
                employee.getName(),
                employee.getDescription(),
                employee.getStartDate(),
                employee.getEndDate(),
                Collections.emptyList()
        );
        return eventRepository.save(newEmployee);
    }

    public void updateEmployee(Event employee) {
        Assert.notNull(employee, "Employee cannot be null");
        this.getEmployee(employee.getId());
        eventRepository.save(employee);
    }

    public List<Guest> getEmployeeSkills(Long employeeId) {
        return guestRepository.findByEventId(employeeId);
    }

    public void deleteEmployee(Long id) {
        Assert.notNull(id, "ID cannot be null");
        eventRepository.delete(id);
    }
}
