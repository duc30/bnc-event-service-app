package com.edgenda.bnc.eventmgmt.service.exception;

public class UnknownGuestException extends RuntimeException {

    public UnknownGuestException(Long id) {
        super("Unknown Skill with ID=" + id);
    }

}
