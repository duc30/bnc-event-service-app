package com.edgenda.bnc.eventmgmt.rest;

import com.edgenda.bnc.eventmgmt.model.Event;
import com.edgenda.bnc.eventmgmt.model.Guest;
import com.edgenda.bnc.eventmgmt.service.EventService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "/events")
public class EventController {

    private final EventService eventService;

    @Autowired
    public EventController(EventService eventService) {
        this.eventService = eventService;
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.GET)
    public Event getEmployee(@PathVariable Long id) {
        return eventService.getEmployee(id);
    }

    @RequestMapping(method = RequestMethod.GET)
    public List<Event> getAllEmployees() {
        return eventService.getEvents();
    }

    @RequestMapping(method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    public Event createEmployee(@RequestBody Event employee) {
        return eventService.createEmployee(employee);
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.PUT)
    public void updateEmployee(@PathVariable Long id, @RequestBody Event employee) {
        eventService.updateEmployee(
                new Event(
                        id,
                        employee.getName(),
                        employee.getDescription(),
                        employee.getStartDate(),
                        employee.getEndDate(),
                        employee.getGuests()
                )
        );
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.DELETE)
    public void deleteEmployee(@PathVariable Long id) {
        eventService.deleteEmployee(id);
    }

    @RequestMapping(path = "/{id}/skills", method = RequestMethod.GET)
    public List<Guest> getEmployeeSkills(@PathVariable Long id) {
        return eventService.getEmployeeSkills(id);
    }

}
