package com.edgenda.bnc.eventmgmt.rest;

import com.edgenda.bnc.eventmgmt.model.Event;
import com.edgenda.bnc.eventmgmt.model.Guest;
import com.edgenda.bnc.eventmgmt.service.GuestService;
import com.edgenda.bnc.eventmgmt.service.exception.UnknownGuestException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/guests")
public class GuestController {

    private final GuestService skillService;

    @Autowired
    public GuestController(GuestService skillService) {
        this.skillService = skillService;
    }

    @RequestMapping(method = RequestMethod.GET)
    public List<Guest> getSkills() {
        return skillService.getSkills();
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<?> getSkill(@PathVariable Long id) {
        try {
            return new ResponseEntity<>(skillService.getSkill(id), HttpStatus.OK);
        } catch (UnknownGuestException e) {
            return new ResponseEntity<>(e, HttpStatus.NOT_FOUND);
        }
    }

    @RequestMapping(path = "/{id}/employees", method = RequestMethod.GET)
    public List<Event> getEmployeesWithSkill(@PathVariable Long id) {
        return skillService.getEmployeesWithSkill(id);
    }
}
