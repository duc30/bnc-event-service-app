package com.edgenda.bnc.eventmgmt.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.data.annotation.PersistenceConstructor;

import javax.persistence.*;
import java.util.List;

@Entity
public class Guest {

    @Id
    @GeneratedValue
    private Long id;

    @NotEmpty
    private String firstName;

    @NotEmpty
    private String lastName;
    
    @Email
    @NotEmpty
    private String email;

	@ManyToMany
    @JoinTable(name = "EVENTS_GUESTS")
    @JsonIgnoreProperties("guests")
    private List<Event> events;

    public Guest() {
    }

    public Guest(Long id, String firstName, String lastName, String email, List<Event> events) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.events = events;
    }

    @PersistenceConstructor
    public Guest(String firstName, String lastName, String email, List<Event> events) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.events = events;
    }

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public List<Event> getEvents() {
		return events;
	}

	public void setEvents(List<Event> events) {
		this.events = events;
	}


}
