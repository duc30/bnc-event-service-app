package com.edgenda.bnc.eventmgmt.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.data.annotation.PersistenceConstructor;

import javax.persistence.*;

import java.util.Date;
import java.util.List;

@Entity
public class Event {

    @Id
    @GeneratedValue
    private Long id;

    @NotEmpty
    private String name;

    @NotEmpty
    private String description;

    @NotEmpty
    private Date startDate;
    
    @NotEmpty
    private Date endDate;

    @ManyToMany(mappedBy = "events")
    @JsonIgnoreProperties({"events", "id"})
    private List<Guest> guests;

    public Event() {
    }

    public Event(Long id, String name, String description, Date startDate, Date endDate, List<Guest> guests) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.startDate = startDate;
        this.endDate = endDate;
        this.guests = guests;
    }

    @PersistenceConstructor
    public Event(String name, String description, Date startDate, Date endDate, List<Guest> guests) {
        this.name = name;
        this.description = description;
        this.startDate = startDate;
        this.endDate = endDate;
        this.guests = guests;
    }



    public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public List<Guest> getGuests() {
		return guests;
	}

	public void setGuests(List<Guest> guests) {
		this.guests = guests;
	}

	@PreRemove
    private void removeGuestsFromEvent() {
        for (Guest guest : guests) {
            guest.getEvents().remove(this);
        }
    }

}
